const express = require('express');
const router = express.Router();

// send our home page
router.all('/', async function (request, response) {
    response.render('login.html', {
        body_class: "login_page",
        page_title: "Workload Generator",
    })
})

// dashboard page
router.all('/dashboard', async function (request, response) {
    response.render('home.html', {
        body_class: "login_page",
        page_title: "Workload Generator",
    })
})

module.exports = router