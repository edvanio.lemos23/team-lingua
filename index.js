const express = require('express')
const handlebars = require('express-handlebars');
const app = express()
const port = 3000

// set html template engine
app.set('view engine', 'handlebars');
app.engine('html', handlebars({
    defaultLayout: 'index',
    extname: '.html',
    layoutsDir: './app/templates',
    partialsDir: './app/templates/partials'
}));

// set html templates directory
app.set('views', './app/templates');
app.set('view cache', false);

// set our assets directory
app.use("/static", express.static(__dirname + '/assets'));

// import routes
app.use(require('./app/routes/index'));

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})